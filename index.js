(() => {
    
    const CONTENT_CONTAINER = document.getElementById('content-container');

    function createNav(DATA) {
        const CONTAINER_NAV = document.getElementById('nav-container');
        CONTAINER_NAV.style.display = 'flex';
        const PAGES = DATA.meta.pagination.limit
        for(let i = 1; i <= PAGES; ++i) {
            const DIV = document.createElement('div');
            DIV.style.flex = `0 1 calc(100%/${PAGES} - 10px`;
            DIV.style.margin = '10px';
            const A = document.createElement('a');
            A.textContent = i;
            A.style.cursor = 'pointer';
            if(i === 1) {
                A.href = 'http://127.0.0.1:5500/index.html';
            }
            else {
                A.href = `index.html?page=${i}`;
            };
            DIV.append(A);
            CONTAINER_NAV.append(DIV);
        };
    };
    
    async function getHeaders(PAGE = null) {
        if(PAGE === null) {
            const RESPONSE = await fetch(`https://gorest.co.in/public-api/posts`);
            const DATA = await RESPONSE.json();
            return DATA;
        };
        const RESPONSE = await fetch(`https://gorest.co.in/public-api/posts?page=${PAGE}`);
        const DATA = await RESPONSE.json();
        return DATA;
    };
    
    function clearPage() {
        while(CONTENT_CONTAINER.firstChild) {
            CONTENT_CONTAINER.firstChild.remove();
        };
    };
    
    function createPage(DATA) {
        DATA.data.forEach(element => {
            const LI = document.createElement('li');
            const HEADER = document.createElement('h3');
            const A = document.createElement('a');
            A.textContent = element.title;
            A.href = `http://127.0.0.1:5500/posts.html?post_id=${element.id}`;
            A.target = "_blank";
            HEADER.append(A);
            //const CONTENT = document.createElement('p');
            //CONTENT.textContent = element.body;
            LI.append(HEADER);
            CONTENT_CONTAINER.append(LI);
        });
    };
    
    document.addEventListener('DOMContentLoaded', async () => {
        // const pageParams = new URLSearchParams(window.location.search);
        // const PAGE = pageParams.get('page');
        // const DATA = await getHeaders(PAGE);
        const RESPONSE = await fetch(`http://127.0.0.1:8000/`);

        console.log(RESPONSE); 
        // clearPage();
        // createNav(DATA);
        // createPage(DATA);
    });
})();