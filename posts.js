(() => {

    const CONTAINER = document.getElementById('container');

    function clearPage() {
        while(CONTAINER.firstChild) {
            CONTAINER.firstChild.remove();
        };
    };

    async function getPost(ID) {
        const RESPONSE = await fetch(`https://gorest.co.in/public-api/posts/${ID}`);
        const DATA = await RESPONSE.json();
        return DATA;
    };


    async function getComments(ID) {
        const RESPONSE = await fetch(`https://gorest.co.in/public-api/comments?post_id=${ID}`);
        const DATA = await RESPONSE.json();
        return DATA;
    };

    function createPost(POST) {
        const POST_CONTAINER = document.createElement('div');
        const HEADER = document.createElement('h3');
        const BODY = document.createElement('p');
        HEADER.textContent = POST.data.title;
        BODY.textContent = POST.data.body;
        POST_CONTAINER.append(HEADER, BODY);
        CONTAINER.append(POST_CONTAINER);
    };

    function createComments(COMMENTS) {
        COMMENTS.data.forEach((person) => {
            const DIV  = document.createElement('div');
            DIV.style.display = 'flex';
            const DIV_LEFT = document.createElement('div');

            DIV_LEFT.style.flex = '0 1 30%';
            const P_NAME = document.createElement('p');
            P_NAME.textContent = person.name;
            const SPAN = document.createElement('span');
            SPAN.textContent = person.email;
            DIV_LEFT.append(P_NAME, SPAN);

            const DIV_RIGTH = document.createElement('div');
            DIV_RIGTH.style.flex = '0 1 70%';
            const P_COMMENT = document.createElement('p');
            P_COMMENT.textContent = person.body;
            DIV_RIGTH.append(P_COMMENT);

            DIV.append(DIV_LEFT, DIV_RIGTH);

            CONTAINER.append(DIV);
        })
    }

    document.addEventListener('DOMContentLoaded', async () => {

        const pageParams = new URLSearchParams(window.location.search);
        const ID = pageParams.get('post_id');
        const POST = await getPost(ID);
        const COMMENTS = await getComments(ID);

        console.log(COMMENTS);
        clearPage();
        createPost(POST)
        createComments(COMMENTS);

    });


})();